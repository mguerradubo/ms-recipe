import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Category } from './category.schema';
import { Model } from 'mongoose';
import axios from 'axios';
import { Category as category, CategoriesResponse, FindRequest, AllCategories} from './categories.pb';
@Injectable()
export class CategoriesService {
  constructor (@InjectModel(Category.name) private categoryModel: Model<Category>){}

  async loadCategory(): Promise<CategoriesResponse>{
    try{
      const response = await axios.get('https://www.themealdb.com/api/json/v1/1/list.php?c=list');
      const categoryData = response.data.meals;
      await Promise.all(
        categoryData.map(async (data) => {
          const category = new this.categoryModel({
            name: data.strCategory,
          })
          await category.save()
        })
      );
      return {success: true, error: undefined};
    }
    catch(error){
      console.log('Error loading categories', error);
      return {success: false, error: error};
    }
    
  }

  async findAll(): Promise<AllCategories>{
    try{
      const categories = await this.categoryModel.find().lean().exec();
      
      const allCategories = categories.map((category) => {
        category.id = category._id.toString();
        return category;
      })
      return {categories: allCategories, error: undefined};
    }catch(error){
      console.log('Error loading categories', error);
      return {categories: undefined, error: error};
    }
  }

  async findOne(request: FindRequest):Promise<category> {
    const category = await this.categoryModel.findOne({_id: request.id}).exec();;
    const categoryData = {
      id: category._id.toString(),
      name: category.name
    }

    return categoryData;
  }

  async findByName(name: string): Promise<Category>{
    return this.categoryModel.findOne({name}).exec()
  }

}
