import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { HydratedDocument } from "mongoose";

export type CategoryDocument = HydratedDocument<Category>

@Schema()
export class Category {
    @Prop({type: String})
    id: string;
    
    @Prop({unique: true})
    name: string;
}
export const CategorySchema = SchemaFactory.createForClass(Category);
CategorySchema.virtual('recipes',{
    ref: 'Recipe',
    localField: 'name',
    foreignField: 'category'
})