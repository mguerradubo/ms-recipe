import { Controller } from '@nestjs/common';
import { CategoriesService } from './categories.service';
import { GrpcMethod } from '@nestjs/microservices';
import {AllCategories, CategoriesResponse, Category , Empty, FindRequest} from './categories.pb';

@Controller('categories')
export class CategoriesController {
  constructor(private readonly categoriesService: CategoriesService) {}

  @GrpcMethod('CategoriesService', 'LoadCategory')
  async loadCategory(): Promise<CategoriesResponse> {
    return this.categoriesService.loadCategory();
  }

  @GrpcMethod('CategoriesService', 'FindAll')
  async findAll(request: Empty): Promise<AllCategories> {
    return this.categoriesService.findAll();
  }

  @GrpcMethod('CategoriesService', 'FindOne')
  async findOne(request: FindRequest): Promise<Category> {
    return this.categoriesService.findOne(request);
  }
}