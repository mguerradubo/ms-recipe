import { Test, TestingModule } from '@nestjs/testing';
import { CategoriesService } from '../categories.service';
import { Model } from 'mongoose';
import { Category } from '../category.schema';
import { getModelToken } from '@nestjs/mongoose';
import { FindRequest } from '../categories.pb';

describe('CategoriesService', () => {
  let service: CategoriesService;
  let categoryModel: Model<Category>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CategoriesService,
        {
          provide: getModelToken(Category.name),
          useValue: {
            find: jest.fn(),
            findOne: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<CategoriesService>(CategoriesService);
    categoryModel = module.get<Model<Category>>(getModelToken(Category.name));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });


  describe('findAll', () => {
    it('should return an array of categories', async () => {
      const mockCategories = [{ id: '1d', name: 'Category 1' }, {id: '2d', name: 'Category 2' }];
      const categoryDocument = mockCategories as any;

      const find: any = {
        exec: jest.fn().mockResolvedValue(categoryDocument),
      };
      

      await jest.spyOn(categoryModel, 'find').mockReturnValue(find);

      const result = await service.findAll();

      expect(result.categories).toBeUndefined();
    });
  });

  describe('findOne', () => {
    it('should find a category by id', async () => {
      const category: FindRequest = {id: '1d'};
      const mockCategory = { _id: "1d", name: 'test' };
  
      const categoryDocument = mockCategory as any;
  
      // Crea un objeto que imite el resultado de findOne
      const findOneResult: any = {
        exec: jest.fn().mockResolvedValue(categoryDocument),
      };
  
      // Espía el método userModel.findOne y devuelve el objeto simulado
      const findOneSpy = jest.spyOn(categoryModel, 'findOne').mockReturnValue(findOneResult);
      
      const result = await service.findOne(category);
  
      expect(result.name).toEqual(categoryDocument.name);
      expect(findOneSpy).toBeDefined();
    });

    
  });

  describe('findByName', () => {
    it('should find a category by name', async () => {
      const categoryName = 'Category 1';
      const mockCategory = { _id: 'someCategoryId', name: categoryName };
      categoryModel.findOne = jest.fn().mockReturnValue({
        exec: jest.fn().mockResolvedValue(mockCategory),
      });

      const result = await service.findByName(categoryName);
      expect(result).toEqual(mockCategory);
    });

    it('should handle errors and reject the promise on failure', async () => {
      // Mock the findOne method of the categoryModel to throw an error
      categoryModel.findOne = jest.fn().mockReturnValue({
        exec: jest.fn().mockRejectedValue(new Error('Test error')),
      });

      await expect(service.findByName('Category 1')).rejects.toThrow('Test error');
    });
  });
});
