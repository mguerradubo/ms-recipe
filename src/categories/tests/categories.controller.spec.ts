import { Test, TestingModule } from '@nestjs/testing';
import { CategoriesController } from '../categories.controller';
import { CategoriesService } from '../categories.service';

describe('CategoriesController', () => {
  let controller: CategoriesController;
  let service: CategoriesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CategoriesController],
      providers: [
        {
          provide: CategoriesService,
          useValue: {
            loadCategory: jest.fn(),
            findAll: jest.fn(),
            findOne: jest.fn(),
            findByName: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<CategoriesController>(CategoriesController);
    service = module.get<CategoriesService>(CategoriesService) as jest.Mocked<CategoriesService>;
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });


    it('should return an array of categories', async () => {
      const mockCategories = [{ id: '1d', name: 'Category 1' }, {id: '2d', name: 'Category 2' }];
      const mockResponse = {categories: mockCategories, error: undefined};
      jest.spyOn(service, 'findAll').mockResolvedValue(mockResponse);
      const result = await controller.findAll({});
      expect(result).toBe(mockResponse);
    });
  
    it('should return a category', async () => {
      const mockCategory = { id: '1d', name: 'Category 1' };
      const mockResponse = mockCategory;
      jest.spyOn(service, 'findOne').mockResolvedValue(mockResponse);
      const result = await controller.findOne({id: '1d'});
      expect(result).toBe(mockResponse);
    });

    
});
