import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { HydratedDocument } from "mongoose";

export type IngredientDocument = HydratedDocument<Ingredient>

@Schema()
export class Ingredient {
    @Prop()
    id: string;
    
    @Prop({unique: true, type: String, required: true, index: 'text' })
    name: string;

    @Prop({default: ''})
    description: string;

    @Prop()
    image: string;
}
export const IngredientSchema = SchemaFactory.createForClass(Ingredient);
IngredientSchema.virtual('recipes',{
    ref: 'Recipe',
    localField: 'name',
    foreignField: 'ingredients'
})