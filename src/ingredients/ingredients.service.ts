import { Injectable } from '@nestjs/common';
import { Ingredient } from './ingredient.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import axios from 'axios';
import {
  Ingredient as ingredient,
  IngredientResponse,
  FindRequest,
  InputLimit,
  AllIngredients,
} from './ingredients.pb';
@Injectable()
export class IngredientsService {
  constructor(
    @InjectModel(Ingredient.name) private ingredientModel: Model<Ingredient>,
  ) {}

  async loadIngredients(): Promise<IngredientResponse> {
    try {
      const response = await axios.get(
        'https://www.themealdb.com/api/json/v1/1/list.php?i=list',
      );
      const ingredientData = response.data.meals;
      await Promise.all(
        ingredientData.map(async (data) => {
          const ingredient = new this.ingredientModel({
            name: data.strIngredient,
            description: data.strDescription || '',
            image: `https://www.themealdb.com/images/ingredients/${data.strIngredient}.png`,
          });
          await ingredient.save();
        }),
      );
      return { success: true, error: undefined };
    } catch (error) {
      console.log('Error loading ingredients', error);
      return { success: false, error: error };
    }
  }

  async findAll(): Promise<AllIngredients> {
    try {
      const ingredients = await this.ingredientModel.find().lean().exec();

      const allIngredients = ingredients.map((ingredient) => {
        ingredient.id = ingredient._id.toString();
        return ingredient;
      });
      return { ingredients: allIngredients, error: undefined };
    } catch (error) {
      console.log('Error loading ingredients', error);
      return { ingredients: undefined, error: error };
    }
  }

  async findOne(request: FindRequest): Promise<ingredient> {
    const ingredient = await this.ingredientModel
      .findOne({ _id: request.id })
      .exec();
    const ingredientData = {
      id: ingredient._id.toString(),
      name: ingredient.name,
      description: ingredient.description,
      image: ingredient.image,
    };

    return ingredientData;
  }

  async findByName(name: string): Promise<ingredient> {
    return this.ingredientModel.findOne({ name: name }).exec();
  }

  async findRandom(request: InputLimit): Promise<AllIngredients> {
    const totalIngredient = await this.ingredientModel.countDocuments();
    const randomSkip = Math.floor(Math.random() * totalIngredient);
    try {
      const ingredients = await this.ingredientModel
        .find()
        .skip(randomSkip)
        .limit(request.limit)
        .exec();

      const allIngredients = ingredients.map((ingredient) => {
        ingredient.id = ingredient._id.toString();
        return ingredient;
      });
      return { ingredients: allIngredients, error: undefined };
    } catch (error) {
      console.log('Error loading random ingredients', error);
      return { ingredients: undefined, error: error };
    }
  }
}
