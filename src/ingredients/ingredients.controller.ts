import { Controller } from '@nestjs/common';
import { IngredientsService } from './ingredients.service';
import { GrpcMethod } from '@nestjs/microservices';
import { FindRequest, Ingredient, IngredientResponse, InputLimit, AllIngredients } from './ingredients.pb';


@Controller('ingredients')
export class IngredientsController {
  constructor(private readonly ingredientsService: IngredientsService) {}

  @GrpcMethod('IngredientsService', 'LoadIngredients')
  async loadIngredients(): Promise<IngredientResponse> {
    return this.ingredientsService.loadIngredients();
  }

  @GrpcMethod('IngredientsService', 'FindAll')
  async findAll(): Promise<AllIngredients> {
    return this.ingredientsService.findAll();
  }

  @GrpcMethod('IngredientsService', 'FindOne')
  async findOne(request: FindRequest): Promise<Ingredient> {
    return this.ingredientsService.findOne(request);
  }

  @GrpcMethod('IngredientsService', 'FindRandom')
  async findRandom(request: InputLimit): Promise<AllIngredients> {
    return this.ingredientsService.findRandom(request);
  } 
}
