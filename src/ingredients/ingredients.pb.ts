/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from "@nestjs/microservices";
import { Observable } from "rxjs";

export const protobufPackage = "ingredients";

export interface Empty {
}

export interface Error {
  message: string;
}

export interface Ingredient {
  id: string;
  name: string;
  description: string;
  image: string;
}

export interface AllIngredients {
  ingredients: Ingredient[];
  error: Error | undefined;
}

export interface InputLimit {
  limit: number;
}

export interface IngredientResponse {
  success: boolean;
  error: Error | undefined;
}

export interface FindRequest {
  id: string;
}

export const INGREDIENTS_PACKAGE_NAME = "ingredients";

export interface IngredientsServiceClient {
  loadIngredients(request: Empty): Observable<IngredientResponse>;

  findAll(request: Empty): Observable<AllIngredients>;

  findOne(request: FindRequest): Observable<Ingredient>;

  findRandom(request: InputLimit): Observable<AllIngredients>;
}

export interface IngredientsServiceController {
  loadIngredients(request: Empty): Promise<IngredientResponse> | Observable<IngredientResponse> | IngredientResponse;

  findAll(request: Empty): Promise<AllIngredients> | Observable<AllIngredients> | AllIngredients;

  findOne(request: FindRequest): Promise<Ingredient> | Observable<Ingredient> | Ingredient;

  findRandom(request: InputLimit): Promise<AllIngredients> | Observable<AllIngredients> | AllIngredients;
}

export function IngredientsServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ["loadIngredients", "findAll", "findOne", "findRandom"];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcMethod("IngredientsService", method)(constructor.prototype[method], method, descriptor);
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcStreamMethod("IngredientsService", method)(constructor.prototype[method], method, descriptor);
    }
  };
}

export const INGREDIENTS_SERVICE_NAME = "IngredientsService";
