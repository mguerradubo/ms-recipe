import { Test, TestingModule } from '@nestjs/testing';
import { IngredientsService } from '../ingredients.service';
import { Model } from 'mongoose';
import { Ingredient } from '../ingredient.schema';
import { getModelToken } from '@nestjs/mongoose';
import { FindRequest } from '../ingredients.pb';

describe('IngredientsService', () => {
  let service: IngredientsService;
  let ingredientModel: Model<Ingredient>

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [IngredientsService,
      {
        provide: getModelToken(Ingredient.name),
        useValue: {
          find: jest.fn(),
          findOne: jest.fn(),
        },
      }
    ],
    }).compile();

    service = module.get<IngredientsService>(IngredientsService);
    ingredientModel = module.get<Model<Ingredient>>(getModelToken(Ingredient.name));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of ingredients', async () => {
      const mockIngredients = [{ id: '1d', name: 'ingredient 1' }, {id: '2d', name: 'ingredient 2' }];
      const ingredientDocument = mockIngredients as any;

      const find: any = {
        exec: jest.fn().mockResolvedValue(ingredientDocument),
      };
      

      await jest.spyOn(ingredientModel, 'find').mockReturnValue(find);

      const result = await service.findAll();

      expect(result.ingredients).toBeUndefined();
    });
  });

  describe('findOne', () => {
    it('should find a ingredient by id', async () => {
      const ingredient: FindRequest = {id: '1d'};
      const mockingredient = { _id: "1d", name: 'test' };
  
      const ingredientDocument = mockingredient as any;
  
      // Crea un objeto que imite el resultado de findOne
      const findOneResult: any = {
        exec: jest.fn().mockResolvedValue(ingredientDocument),
      };
  
      // Espía el método userModel.findOne y devuelve el objeto simulado
      const findOneSpy = jest.spyOn(ingredientModel, 'findOne').mockReturnValue(findOneResult);
      
      const result = await service.findOne(ingredient);
  
      expect(result.name).toEqual(ingredientDocument.name);
      expect(findOneSpy).toBeDefined();
    });
  
  });
});

