import { Test, TestingModule } from '@nestjs/testing';
import { IngredientsController } from '../ingredients.controller';
import { IngredientsService } from '../ingredients.service';

describe('IngredientsController', () => {
  let controller: IngredientsController;
  let service: IngredientsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [IngredientsController],
      providers: [
        {
          provide: IngredientsService,
          useValue: {
            loadIngredient: jest.fn(),
            findAll: jest.fn(),
            findOne: jest.fn(),
            findByName: jest.fn(),
          },
        }
      ],
    }).compile();

    controller = module.get<IngredientsController>(IngredientsController);
    service = module.get<IngredientsService>(IngredientsService) as jest.Mocked<IngredientsService>;
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return an array of ingredients', async () => {
    const mockingredients = [{ id: '1d', name: 'ingredient 1' , description: 'description 1', Image: 'image1'}, {id: '2d', name: 'ingredient 2', description: 'description 2', image: 'image2'}];
    const mockResponse = {ingredients: mockingredients, error: undefined};
    const document = mockingredients as any;
    jest.spyOn(service, 'findAll').mockResolvedValue(document);
    const result = await controller.findAll();
    expect(result).toBe(mockResponse.ingredients);
  });

  it('should return a ingredient', async () => {
    const mockingredient = { id: '1d', name: 'ingredient 1' , description: 'description 1', Image: 'image1'};
    const mockResponse = mockingredient;
    const document = mockingredient as any;
    jest.spyOn(service, 'findOne').mockResolvedValue(document);
    const result = await controller.findOne({id: '1d'});
    expect(result).toBe(mockResponse);
  });

});
