import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Recipe } from './recipe.schema';
import { Model } from 'mongoose';
import axios from 'axios';
import { IngredientsService } from '../ingredients/ingredients.service';
import { CategoriesService } from '../categories/categories.service';
import { Recipe as recipe,  InputLetter, RecipesResponse, InputQuery, InputLimit, InputCategory, FindRequest, AllRecipes} from './recipes.pb';
import { Comment } from 'src/comments/comment.schema';

@Injectable()
export class RecipesService {
  constructor(
    @InjectModel(Recipe.name) private recipeModel: Model<Recipe>,
    private ingredientModel: IngredientsService,
    private categoryModel: CategoriesService,
    ){}
    async loadRecipes(request: InputLetter): Promise<RecipesResponse> {
      try {
        const response = await axios.get(`https://www.themealdb.com/api/json/v1/1/search.php?f=${request.letter}`);
        const recipesData = response.data.meals;
    
        for (const data of recipesData) {
          const ingredients = [];
          const measures = [];
          const category = await this.categoryModel.findByName(data.strCategory);
          // Dynamically create ingredients and measures arrays
          for (let i = 1; i <= 20; i++) {
            const ingredient = data[`strIngredient${i}`];
            const measure = data[`strMeasure${i}`];
    
            if (ingredient && measure) {
              const capitalizedIngredientName = ingredient.charAt(0).toUpperCase() + ingredient.slice(1);

              const ingredientObj = await this.ingredientModel.findByName(capitalizedIngredientName);
    
              if (category && ingredientObj) {
                ingredients.push(ingredientObj);
                measures.push(measure);
              }
            }
          }
    
          const recipe = new this.recipeModel({
            name: data.strMeal,
            category: category.name, // Usar el objeto completo de categoría
            instruction: data.strInstructions,
            image: data.strMealThumb || '',
            video: data.strYoutube || '',
            ingredients: ingredients, // Usar los objetos completos de ingredientes
            measure: measures,
          });
    
          await recipe.save();
        }
        return {success: true, error: undefined};
      } catch (error) {
        console.log('Error loading recipes', error);
        return {success: false, error: error};
      }
    }
    

  async findAll(): Promise<AllRecipes>{
    try{
      const recipes = await this.recipeModel.find().lean().exec();
      
      const allRecipe = recipes.map((recipe) => {
        recipe.id = recipe._id.toString();  
        return recipe;
      })
      return {recipes: allRecipe, error: undefined};
    }catch(error){
      console.log('Error loading recipes', error);
      return {recipes: undefined, error: error};
    }
  }

  async findOne(request: FindRequest): Promise<recipe> {
    const recipe = await this.recipeModel.findOne({_id: request.id}).exec();;
    const recipeData = {
      id: recipe._id.toString(),
      name: recipe.name,
      category: recipe.category,
      instruction: recipe.instruction,
      image: recipe.image,
      video: recipe.video,
      ingredients: recipe.ingredients,
      measure: recipe.measure,
      comments: recipe.comments,
    }

    return recipeData;
  }

  async findRandom(request: InputLimit): Promise<AllRecipes> {
    const totalRecipes = await this.recipeModel.countDocuments();
    const randomSkip = Math.floor(Math.random() * totalRecipes);
  try{
    const recipes = await this.recipeModel
    .find()
    .skip(randomSkip)
    .limit(request.limit)
    .exec();
      
      const allRecipe = recipes.map((recipe) => {
        recipe.id = recipe._id.toString();
        return recipe;
      })
      return {recipes: allRecipe, error: undefined};
    }catch(error){
      console.log('Error loading random recipes', error);
      return {recipes: undefined, error: error};
    }
  }

  async findCategoryRecipes(request: InputCategory): Promise<AllRecipes> {
    try{
      const recipes = await this.recipeModel.find({category: request.category})
      
      const allRecipe = recipes.map((recipe) => {
        recipe.id = recipe._id.toString();
        return recipe;
      })
      return {recipes: allRecipe, error: undefined};
    }catch(error){
      console.log('Error loading category recipes', error);
      return {recipes: undefined, error: error};
    }
}
  
  async searchRecipes(request: InputQuery): Promise<AllRecipes> {
      try{
        const recipes= await this.recipeModel
        .find({ name: { $regex: request.query, $options: 'i' } });

      const allRecipe = recipes.map((recipe) => {
        recipe.id = recipe._id.toString();
        return recipe;
      })
      return {recipes: allRecipe, error: undefined};
    }catch(error){
      console.log('Error loading search recipes', error);
      return {recipes: undefined, error: error};
    }

  }
  async updateRecipeWithComment(recipeId: string, comments: Comment): Promise<recipe> {
    const updatedRecipe = await this.recipeModel.findByIdAndUpdate(
      recipeId,
      { $push: { comments } },
      { new: true } // Devuelve el usuario actualizado
    );

    return updatedRecipe;
  }

  async updateRecipeWithoutComment(recipeId: string, comments: Comment): Promise<recipe> {
    // Encuentra el usuario por su _id y actualiza el campo "comments" para eliminar la referencia al comentario
    const updatedUser = await this.recipeModel.findByIdAndUpdate(
      recipeId,
      { $pull: { comments: {_id: comments.id} } }, // Utiliza $pull para eliminar el comentario del arreglo
      { new: true } // Devuelve el usuario actualizado
    );
  
    return updatedUser;
  }
  
  async findRecipeComments(recipeId: string): Promise<Recipe> {
    return await this.recipeModel.findOne({_id: recipeId}).exec();
  }
}