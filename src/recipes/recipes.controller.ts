import { Controller } from '@nestjs/common';
import { RecipesService } from './recipes.service';
import { GrpcMethod } from '@nestjs/microservices';
import { InputCategory, InputLetter, InputLimit, InputQuery, Recipe, RecipesResponse, AllRecipes, FindRequest, Empty} from './recipes.pb';

@Controller('recipes')
export class RecipesController {
  constructor(private readonly recipesService: RecipesService) {}

  @GrpcMethod('RecipesService', 'LoadRecipes')
  async loadRecipes(request: InputLetter): Promise<RecipesResponse> {
    return this.recipesService.loadRecipes(request);
  }

  @GrpcMethod('RecipesService', 'FindAll')
  async findAll(request: Empty): Promise<AllRecipes> {
    return this.recipesService.findAll();
  }

  @GrpcMethod('RecipesService', 'FindOne')
  async findOne(request: FindRequest): Promise<Recipe> {   
    return this.recipesService.findOne(request);
  }

  @GrpcMethod('RecipesService', 'FindRandom')
  async findRandom(request: InputLimit): Promise<AllRecipes> {   
    return this.recipesService.findRandom(request);
  }

  @GrpcMethod('RecipesService', 'FindCategoryRecipes')
  async findCategoryRecipes(request: InputCategory): Promise<AllRecipes> {   
    return this.recipesService.findCategoryRecipes(request);
  }

  @GrpcMethod('RecipesService', 'SearchRecipes')
  async searchRecipes(request: InputQuery): Promise<AllRecipes> {
    return this.recipesService.searchRecipes(request);
  }
}
