import { Test, TestingModule } from '@nestjs/testing';
import { RecipesController } from '../recipes.controller';
import { RecipesService } from '../recipes.service';
import { IngredientsService } from '../../ingredients/ingredients.service';
import { CategoriesService } from '../../categories/categories.service';
describe('RecipesController', () => {
  let controller: RecipesController;
  let service: RecipesService;
  let ingredientsService: IngredientsService;
  let categoriesService: CategoriesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RecipesController],
      providers: [RecipesService,
        {
          provide: RecipesService,
          useFactory: () => ({
            loadRecipes: jest.fn(),
            findAll: jest.fn(),
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
          }),
        },
        {
          provide: IngredientsService,
          useValue: {
            find: jest.fn(),
            findByName: jest.fn(),
          },
        },
        {
          provide: CategoriesService,
          useValue: {
            findByName: jest.fn(),
            find: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<RecipesController>(RecipesController);
    service = module.get<RecipesService>(RecipesService) as jest.Mocked<RecipesService>;
    ingredientsService = module.get<IngredientsService>(IngredientsService) as jest.Mocked<IngredientsService>;
    categoriesService = module.get<CategoriesService>(CategoriesService) as jest.Mocked<CategoriesService>;
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should find all recipes', async () => {
    const mockResponse = [{id: '1d',name: 'Recipe 1', category: 'Category 1', instruction: 'Instruction 1', image: 'Image 1', video: 'Video 1', ingredients: ['Ingredient 1'], measure: ['Measure 1']}];
    const mockRecipe = {recipes: mockResponse, error: undefined};

    const find: any = {
      exec: jest.fn().mockResolvedValue(mockResponse as any),
    };

    jest.spyOn(service, 'findAll').mockResolvedValue(mockRecipe as any);

    const result = await controller.findAll({});
    expect(result).toBe(mockRecipe);
  });
});
