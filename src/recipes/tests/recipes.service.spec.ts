import { Test, TestingModule } from '@nestjs/testing';
import { RecipesService } from '../recipes.service';
import { Model } from 'mongoose';
import { Recipe } from '../recipe.schema';
import { CategoriesService } from '../../categories/categories.service';
import { IngredientsService } from '../../ingredients/ingredients.service';
import { getModelToken } from '@nestjs/mongoose';

describe('RecipesService', () => {
  let service: RecipesService;
  let recipeModel: Model<Recipe>;
  let categoryService: CategoriesService;
  let ingredientService: IngredientsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RecipesService,
        {
          provide: getModelToken(Recipe.name),
          useValue: {
            find: jest.fn(),
            findOne: jest.fn(),
            save: jest.fn(),
          },
        },
        {
          provide: CategoriesService,
          useValue: {
            findByName: jest.fn(),
            find: jest.fn(),
          },
        },
        {
          provide: IngredientsService,
          useValue: {
            find: jest.fn(),
            findByName: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<RecipesService>(RecipesService);
    recipeModel = module.get<Model<Recipe>>(getModelToken(Recipe.name));
    categoryService = module.get<CategoriesService>(CategoriesService);
    ingredientService = module.get<IngredientsService>(IngredientsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should find all recipes', async () => {
    const mockResponse = [{id: '1d',name: 'Recipe 1', category: 'Category 1', instruction: 'Instruction 1', image: 'Image 1', video: 'Video 1', ingredients: ['Ingredient 1'], measure: ['Measure 1']}];
    const mockRecipe = {recipes: mockResponse, error: undefined};

    const find: any = {
      exec: jest.fn().mockResolvedValue(mockResponse as any),
    };

    jest.spyOn(recipeModel, 'find').mockResolvedValue(find);
    const result = await service.findAll();
    expect(result.recipes).toBeUndefined();
  })
});
