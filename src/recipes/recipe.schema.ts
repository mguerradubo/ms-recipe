import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { HydratedDocument } from "mongoose";
import * as mongoose from 'mongoose';
import { Ingredient } from "../ingredients/ingredient.schema";
import { Comment } from "../comments/comment.schema";

export type RecipeDocument = HydratedDocument<Recipe>

@Schema()
export class Recipe {
    @Prop({type: String})
    id: string

    @Prop({unique: true, type: String, required: true, index: 'text' })
    name: string;

    @Prop()
    category: string;

    @Prop()
    instruction: string;

    @Prop([{type: mongoose.Schema}])
    ingredients: [Ingredient];

    @Prop([String])
    measure: string[];

    @Prop()
    image: string;

    @Prop()
    video: string;

    @Prop([{ type: mongoose.Schema }])
    comments: [Comment];

}
export const RecipeSchema = SchemaFactory.createForClass(Recipe);