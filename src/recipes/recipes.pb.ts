/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from "@nestjs/microservices";
import { Observable } from "rxjs";
import { Comment } from "./comments.pb";
import { Ingredient } from "./ingredients.pb";

export const protobufPackage = "recipes";

export interface Empty {
}

export interface Recipe {
  id: string;
  name: string;
  category: string;
  instruction: string;
  ingredients: Ingredient[];
  measure: string[];
  image: string;
  video: string;
  comments: Comment[];
}

export interface AllRecipes {
  recipes: Recipe[];
  error: Error | undefined;
}

export interface Error {
  message: string;
}

export interface InputQuery {
  query: string;
}

export interface InputLetter {
  letter: string;
}

export interface InputCategory {
  category: string;
}

export interface InputLimit {
  limit: number;
}

export interface RecipesResponse {
  success: boolean;
  error: Error | undefined;
}

export interface FindRequest {
  id: string;
}

export const RECIPES_PACKAGE_NAME = "recipes";

export interface RecipesServiceClient {
  loadRecipes(request: InputLetter): Observable<RecipesResponse>;

  findAll(request: Empty): Observable<AllRecipes>;

  findOne(request: FindRequest): Observable<Recipe>;

  findRandom(request: InputLimit): Observable<AllRecipes>;

  findCategoryRecipes(request: InputCategory): Observable<AllRecipes>;

  searchRecipes(request: InputQuery): Observable<AllRecipes>;
}

export interface RecipesServiceController {
  loadRecipes(request: InputLetter): Promise<RecipesResponse> | Observable<RecipesResponse> | RecipesResponse;

  findAll(request: Empty): Promise<AllRecipes> | Observable<AllRecipes> | AllRecipes;

  findOne(request: FindRequest): Promise<Recipe> | Observable<Recipe> | Recipe;

  findRandom(request: InputLimit): Promise<AllRecipes> | Observable<AllRecipes> | AllRecipes;

  findCategoryRecipes(request: InputCategory): Promise<AllRecipes> | Observable<AllRecipes> | AllRecipes;

  searchRecipes(request: InputQuery): Promise<AllRecipes> | Observable<AllRecipes> | AllRecipes;
}

export function RecipesServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      "loadRecipes",
      "findAll",
      "findOne",
      "findRandom",
      "findCategoryRecipes",
      "searchRecipes",
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcMethod("RecipesService", method)(constructor.prototype[method], method, descriptor);
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcStreamMethod("RecipesService", method)(constructor.prototype[method], method, descriptor);
    }
  };
}

export const RECIPES_SERVICE_NAME = "RecipesService";
