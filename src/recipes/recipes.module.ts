import { Module } from '@nestjs/common';
import { RecipesService } from './recipes.service';
import { RecipesController } from './recipes.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Recipe, RecipeSchema } from './recipe.schema';
import { IngredientsModule } from '../ingredients/ingredients.module';
import { CategoriesModule } from '../categories/categories.module';
@Module({
  imports: [MongooseModule.forFeature([{name: Recipe.name, schema: RecipeSchema}]), IngredientsModule, CategoriesModule],
  controllers: [RecipesController],
  providers: [RecipesService],
  exports: [RecipesService]
})
export class RecipesModule {}
