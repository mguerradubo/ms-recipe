import { Module } from '@nestjs/common';
import { RecipesModule } from './recipes/recipes.module';
import { MongooseModule } from '@nestjs/mongoose';
import { CategoriesModule } from './categories/categories.module';
import { IngredientsModule } from './ingredients/ingredients.module';
import { CommentsModule } from './comments/comments.module';
import * as dotenv from 'dotenv';

dotenv.config()
@Module({
  imports: [
    MongooseModule.forRoot(
      process.env.URI,
      {autoIndex: true}
    ),
    RecipesModule,
    CategoriesModule,
    IngredientsModule,
    CommentsModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
