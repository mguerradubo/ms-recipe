import { Test, TestingModule } from '@nestjs/testing';
import { CommentsService } from '../comments.service';
import { Model } from 'mongoose';
import { Comment } from '../comment.schema';
import { getModelToken } from '@nestjs/mongoose';
import { CommentRequest, RemoveRequest } from '../comments.pb';
import { RecipesService } from '../../recipes/recipes.service';
describe('CommentsService', () => {
  let service: CommentsService;
  let commentModel: Model<Comment>;
  let recipeService: RecipesService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CommentsService, 
      {
        provide: getModelToken(Comment.name),
        useValue: {
          find: jest.fn(),
          findOne: jest.fn(),
        },
      },
      {
        provide: RecipesService,
        useValue: {
          findOne: jest.fn(),
          create: jest.fn(),
          findByIdAndRemove: jest.fn(),
          find: jest.fn(),
        },
      },
      ],
    }).compile();

    service = module.get<CommentsService>(CommentsService);
    commentModel = module.get<Model<Comment>>(getModelToken(Comment.name));
    recipeService = module.get<RecipesService>(RecipesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a comment', async () => {
    const mockComment: CommentRequest = { email: 'test@gmail.com', text: 'Comment 1' , recipeId: '1d'};
    const mockResponse = mockComment as any;

    const comment = new Comment();

    
    jest.spyOn(service, 'createComment').mockResolvedValue(mockResponse);
    const result = await service.createComment(mockComment);
    expect(result).toBe(mockResponse);
  });

  it('should delete a comment', async () => {
    const mockComment: RemoveRequest = { id: '1d', recipeId: '1rd'};
    const mockResponse = mockComment as any;
  
    const comment = new Comment();
    
    jest.spyOn(service, 'deleteComment').mockResolvedValue(mockResponse);
    const result = await service.deleteComment(mockComment);
    expect(result).toBe(mockResponse);
  });

  it('should find all comments', async () => {
    const mockComment = [{ id: '1d', name: 'Comment 1', recipeId: 'r1' }, {id: '2d', name: 'Comment 2', recipeId: 'r2'}];
    
    const commentDocument = mockComment as any;

    const find: any = {
      exec: jest.fn().mockResolvedValue(commentDocument),
    };

    await jest.spyOn(commentModel, 'find').mockReturnValue(find);

      const result = await service.findAll();

      expect(result.comments).toBeUndefined();
  });
});
