import { Test, TestingModule } from '@nestjs/testing';
import { CommentsController } from '../comments.controller';
import { CommentsService } from '../comments.service';
import { RecipesService } from '../../recipes/recipes.service';
import { CommentRequest } from '../comments.pb';

describe('CommentsController', () => {
  let controller: CommentsController;
  let service: CommentsService;
  let recipeService: RecipesService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CommentsController],
      providers: [CommentsService,
        {
          provide: CommentsService,
          useFactory: () => ({
            createComment: jest.fn(),
            deleteComment: jest.fn(),
            findAll: jest.fn(),})
        },
        {
          provide: RecipesService,
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<CommentsController>(CommentsController);
    service = module.get<CommentsService>(CommentsService) as jest.Mocked<CommentsService>;
    recipeService = module.get<RecipesService>(RecipesService) as jest.Mocked<RecipesService>;
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create a comment', async () => {
    const mockComment: CommentRequest = { text: 'Comment 1', recipeId: 'r1', email: 'test@gmail'}
    
    const comment = {...mockComment, id: '1d'}
    const mockResponse = comment as any;
    jest.spyOn(service, 'createComment').mockResolvedValue(mockResponse);

    const result = await controller.createComment(mockComment);
    expect(result).toBe(mockResponse);
  });

  it('should delete a comment', async () => {
    const mockComment = { id: '1d', text: 'Comment 1', recipeId: 'r1', email: 'test@gmail'}
    const mockResponse = {success: true, error: undefined};
    jest.spyOn(service, 'deleteComment').mockResolvedValue(mockResponse);
    const result = await controller.deleteComment(mockComment);
    expect(result).toBe(mockResponse);
  });

  it('should find all comments', async () => {
    const mockComment = [{ id: '1d', text: 'Comment 1', recipeId: 'r1', email: 'test1@gmail.com', createdAt: ''}, {id: '2d', text: 'Comment 2', recipeId: 'r2', email: 'test2@gmail.com', createdAt: ''}];
    const mockResponse = {comments: mockComment, error: undefined};
    await jest.spyOn(service, 'findAll').mockResolvedValue(mockResponse);
    const result = await controller.findAll({});
    expect(result).toBe(mockResponse);
  });
});
