import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { RecipesService } from '../recipes/recipes.service';
import { Comment } from './comment.schema';
import { Comment as comment, CommentRequest, RemoveRequest, CommentsResponse, AllComments} from './comments.pb';
@Injectable()
export class CommentsService {
    constructor(
        @InjectModel(Comment.name) private commentModel: Model<Comment>,
        private recipeModel: RecipesService,
    ){}

    async createComment(request: CommentRequest): Promise<comment> {
        const recipeId = {id: request.recipeId};
        const recipe = await this.recipeModel.findOne(recipeId);
        const comment = new this.commentModel({ text: request.text, email: request.email, recipeId: recipe.id });
        
        comment.save();
        
        await this.recipeModel.updateRecipeWithComment(recipe.id, comment);

        
        const commentData = {
          id: comment._id.toString(),
          text: comment.text,
          email: comment.email,
          recipeId: comment.recipeId,
          createdAt: comment.createdAt,
        }

        return commentData;
      }

      async deleteComment(request: RemoveRequest): Promise<CommentsResponse> {
        const comment = await this.commentModel.findOne({_id: request.id}).exec();
        const recipe = await this.recipeModel.findOne({id: request.recipeId});
      
        if (comment && recipe) {
          await this.recipeModel.updateRecipeWithoutComment(request.recipeId, comment);

          await this.commentModel.findByIdAndDelete(request.id).exec();
          return {success: true, error: undefined}; 
        } else {
          return {success: false, error: {message:'Delete not found'}}; 
        }
      }
      
      async findAll(): Promise<AllComments>{
        try{
          const comments = await this.commentModel.find().lean().exec();
          
          const allComments = comments.map((comment) => {
            comment.id = comment._id.toString();
            return comment;
          })
          return {comments: allComments, error: undefined};
        }catch(error){
          console.log('Error loading comments', error);
          return {comments: undefined, error: error};
        }
      }


}
