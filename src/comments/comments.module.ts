import { Module } from '@nestjs/common';
import { CommentsService } from './comments.service';
import { CommentsController } from './comments.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Comment, CommentSchema } from './comment.schema';
import { RecipesModule } from '../recipes/recipes.module';

@Module({
  imports:[MongooseModule.forFeature([{name: Comment.name, schema: CommentSchema}]),  RecipesModule],
  controllers: [CommentsController],
  providers: [CommentsService],
  exports: [CommentsService]
})
export class CommentsModule {}
