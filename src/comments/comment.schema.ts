import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { HydratedDocument } from "mongoose";
import * as mongoose from "mongoose";
import { Recipe } from "src/recipes/recipe.schema";

export type CommentDocument = HydratedDocument<Comment>;

@Schema()
export class Comment {
    @Prop({type: String})
    id: string;

    @Prop({ required: true })
    text: string;

    @Prop({required: true})
    email: string;

    @Prop()
    recipeId: string;
    
    @Prop({type: Date, default: Date.now })
    createdAt: string;
}

export const CommentSchema = SchemaFactory.createForClass(Comment);
