import { Controller } from '@nestjs/common';
import { CommentsService } from './comments.service';
import { GrpcMethod } from '@nestjs/microservices';
import { AllComments, Comment, CommentRequest, CommentsResponse, Empty, RemoveRequest } from './comments.pb';

@Controller('comments')
export class CommentsController {
  constructor(private readonly commentsService: CommentsService) {}

  @GrpcMethod('CommentsService', 'CreateComment')
  async createComment(request: CommentRequest): Promise<Comment> {
    return this.commentsService.createComment(request);
  }

  @GrpcMethod('CommentsService', 'DeleteComment')
  async deleteComment(request: RemoveRequest): Promise<CommentsResponse> {
    return this.commentsService.deleteComment(request);
  }

  @GrpcMethod('CommentsService', 'FindAll')
  async findAll(request: Empty): Promise<AllComments> {
    return this.commentsService.findAll();
  }
}
