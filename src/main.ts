import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { protobufPackage as recipesPackage } from './recipes/recipes.pb';
import { protobufPackage as commentsPackage } from './comments/comments.pb';
import { protobufPackage as ingredientsPackage } from './ingredients/ingredients.pb';
import { protobufPackage as categoriesPackage } from './categories/categories.pb';
import * as dotenv from 'dotenv';

dotenv.config()
async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule, 
    {
      transport: Transport.GRPC,
      options: {
        url: process.env.PORT,
        package: [
          recipesPackage,
          commentsPackage,
          ingredientsPackage,
          categoriesPackage,
        ],
        protoPath: [
          join('node_modules/protos/proto/recipes.proto'),
          join('node_modules/protos/proto/comments.proto'),
          join('node_modules/protos/proto/ingredients.proto'),
          join('node_modules/protos/proto/categories.proto'),]
      }
    }
  );
  await app.listen();
  console.log("[*] Awaiting GRPC requests");
}
bootstrap();
